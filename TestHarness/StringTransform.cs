﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace TestHarness
{
  public class StringTransform
  {
    public static string TransformSentence(string sentence)
    {
      string pattern = @"[^a-z]+";
      // input = "Abc1234Def,5678Ghi9 @ bmnfg 012 Jklm uu a";
      string[] words = Regex.Split(sentence, pattern,
                                    RegexOptions.IgnoreCase,
                                    TimeSpan.FromMilliseconds(500));

      string[] tranformedWords = new string[words.Length];

      for (int i = 0; i < words.Length; i++)
      {
        string originalWord = words[i];
        if (!string.IsNullOrEmpty(originalWord))
        {
          string tranformedWord = TransformWord(originalWord);
          int loc = sentence.IndexOf(originalWord);
          if (loc >= 0)
          {
            sentence = sentence.Remove(loc, originalWord.Length).Insert(loc, tranformedWord);
          }
        }
      }

      return sentence;  
    }

    protected static string TransformWord(string word)
    {
      string result;
      if (word.Length == 0)
      {
        result = "0";
      }

      else if (word.Length > 1)
      {
        char[] chars = word.ToCharArray();
        List<char> uniqueChars = new List<char>();
        for (int i = 1; i < chars.Length - 1; i++)
        {
          if (!uniqueChars.Contains(chars[i]))
          {
            uniqueChars.Add(chars[i]);
          }
        }
        result = string.Concat(chars[0], uniqueChars.Count, chars[chars.Length - 1]);
      }
      else
      {
        result = string.Concat(word, '0');
      }

      return result;

    }

  }
}
