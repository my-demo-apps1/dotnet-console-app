﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections;
using TestHarness.Extenstions;

namespace TestHarness
{
  public class Node
  {
    public int Data;
    public Node Left;
    public Node Right;
    public void DisplayNode()
    {
      Console.WriteLine(Data + " ");
    }
  }

  public class BinarySearchTree
  {

    public Node root;
    public BinarySearchTree()
    {
      root = null;
    }

    public void Insert(int i)
    {

      InsertRec(ref root, i);
      //Node newNode = new Node();
      //newNode.Data = i;
      //if (root == null)
      //    root = newNode;
      //else
      //{
      //    Node current = root;
      //    Node parent;
      //    while (true)
      //    {
      //        parent = current;
      //        if (i < current.Data)
      //        {
      //            current = current.Left;
      //            if (current == null)
      //            {
      //                parent.Left = newNode;
      //                break;
      //            }
      //        }
      //        else
      //        {
      //            current = current.Right;
      //            if (current == null)
      //            {
      //                parent.Right = newNode;
      //                break;
      //            }
      //        }
      //    }
      //}
    }

    public void InsertRec(ref Node current, int i)
    {

      if (current == null)
      {
        Node newNode = new Node();
        newNode.Data = i;
        current = newNode;
      }
      else
      {
        if (i <= current.Data)
        {
          InsertRec(ref current.Left, i);
        }
        else
        {
          InsertRec(ref current.Right, i);
        }
      }

    }

    public void PrintTree(Node current = null, string pad = "")
    {
      if (current == null)
        current = root;


      Console.WriteLine(pad + current.Data);
      if (current.Left != null)
      {

        PrintTree(current.Left, pad + "<<");

      }
      if (current.Right != null)
      {
        PrintTree(current.Right, pad + ">>");
      }


    }

    // sorted
    public void PrintInOder(Node current)
    {
      if (current != null)
      {
        if (current.Left != null)
        {
          PrintInOder(current.Left);
        }

        Console.WriteLine(current.Data);

        if (current.Right != null)
        {
          PrintInOder(current.Right);
        }
      }
    }

    public void GetInOder(Node current, ref Node[] nodesInOrder)
    {
      if (current != null)
      {
        if (current.Left != null)
        {
          PrintInOder(current.Left);
        }

        nodesInOrder.Append(current);

        if (current.Right != null)
        {
          PrintInOder(current.Right);
        }
      }
    }

    public void BalanceTree()
    {
      Node[] nodesInOder = { };
      GetInOder(root, ref nodesInOder);
      BalanceTree(nodesInOder, 0, nodesInOder.Length);

    }

    private void BalanceTree(Node[] nodesInOder, int minIndex, int maxIndex)
    {
      // Node[] nodesInOder = { };
      // GetInOder(root, ref nodesInOder);


    }

  }

}