﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestHarness.Arrays;

namespace TestHarness
{
  class Program
  {
    static void Main(string[] args)
    {
      int[] sortedArray = { 2, 3, 7, 11, 13, 19 };
      int result = SearchAlgorithms.BinarySearchDisplay(sortedArray, 11);

      // Start();
    }

    private static void Start()
    {
      Console.WriteLine("Type in a sentence to tranform:");
      Console.WriteLine("(Enter 'q' to quit)");
      string inputString = Console.ReadLine();
      if (!String.Equals(a: inputString, b: "q", comparisonType: StringComparison.OrdinalIgnoreCase))
      {
        string transformedSentence = StringTransform.TransformSentence(inputString);
        Console.WriteLine("Transformed Sentence: {0}", transformedSentence);
        Console.WriteLine("Press any key to continue...");
        Console.ReadLine();
        Start();
      }
     
    }
  }
}

#region "other stuff"
/*
 * 
 *  // int[] orderedList = { 1,2,3,5, 12, 13, 14, 14, 30, 41, 52, 60, 67, 67, 68, 69 };
            // var result = SearchAlgorithms.BinarySearchDisplay(orderedList, 67);
            /*
            BinarySearchTree nums = new BinarySearchTree();
            nums.Insert(50);
            nums.Insert(17);
            nums.Insert(23);
            nums.Insert(12);
            nums.Insert(19);
            nums.Insert(54);
            nums.Insert(9);
            nums.Insert(14);
            nums.Insert(67);
            nums.Insert(76);
            nums.Insert(72);
            nums.Insert(53);
            // nums.PrintTree();
            nums.PrintInOder(nums.root);
        

int[] a = { 7, 1, 2, 3, 4, 5, 6 };
var balancedArray = BalancedArray.BalanceArray(a);
Console.WriteLine(balancedArray);
 *
 * System.out.println((int) distance(ENDx - STARTx, ENDy - STARTy));    
public static double distance(int x, int y) {
        // axes symmetry
        x = Math.abs(x);
        y = Math.abs(y);
        // diagonal symmetry
        if (x < y) {
            int t = x;
            x = y;
            y = t;
        }
        // 2 corner cases
        if (x == 1 && y == 0) {
            return 3;
        }
        if (x == 2 && y == 2) {
            return 4;
        }

        // main formula
        int delta = x - y;
        if (y > delta) {
            return (delta - 2 * Math.floor((float) (delta - y) / 3));
        } else {
            return (delta - 2 * Math.floor((delta - y) / 4));
        }
    }
 */
#endregion