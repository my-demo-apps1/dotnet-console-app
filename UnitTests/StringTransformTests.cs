﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestHarness;

namespace TestHarness.Tests
{
  [TestClass()]
  public class StringTransformTests
  {
    [TestMethod()]
    public void TransformSentenceTest_EmptyInput()
    {
      string inputString = "";
      string transformedString = StringTransform.TransformSentence(inputString);
      Assert.AreEqual(transformedString, "");
    }

    [TestMethod()]
    public void TransformSentenceTest_HelloWorld()
    {
      string inputString = "Hello World!";
      string transformedString = StringTransform.TransformSentence(inputString);
      Assert.AreEqual(transformedString, "H2o W3d!");
    }

    [TestMethod()]
    public void TransformSentenceTest_SingleWord()
    {
      string inputString = "Hello";
      string transformedString = StringTransform.TransformSentence(inputString); 
      Assert.AreEqual(transformedString, "H2o");
    }

    [TestMethod()]
    public void TransformSentenceTest_SingleLetter()
    {
      string inputString = "H";
      string transformedString = StringTransform.TransformSentence(inputString);
      Assert.AreEqual(transformedString, "H0");
    }

    [TestMethod()]
    public void TransformSentenceTest_TwoSingleLetters()
    {
      string inputString = "H W";
      string transformedString = StringTransform.TransformSentence(inputString);
      Assert.AreEqual(transformedString, "H0 W0");
    }

    [TestMethod()]
    public void TransformSentenceTest_NoLetter()
    {
      string inputString = "!123";
      string transformedString = StringTransform.TransformSentence(inputString);
      Assert.AreEqual(transformedString, "!123");
    }

    [TestMethod()]
    public void TransformSentenceTest_Complex()
    {
      string inputString = "Hello123World$Welcome. To";
      string transformedString = StringTransform.TransformSentence(inputString);
      Assert.AreEqual(transformedString, "H2o123W3d$W5e. T0o");
    }
  }
}

