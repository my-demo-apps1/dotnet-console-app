﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestHarness.Arrays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHarness.Arrays.Tests
{
  [TestClass()]
  public class SearchAlgorithmsTests
  {
    [TestMethod()]
    public void BinarySearchDisplayTest_NotFound()
    {
      int[] sortedArray = { 2, 3, 7, 11, 13, 19 };
      int result = SearchAlgorithms.BinarySearchDisplay(sortedArray, 20);
      Assert.AreEqual(result, -1);
    }

    [TestMethod()]
    public void BinarySearchDisplayTest_Found()
    {
      int[] sortedArray = { 2, 3, 7, 11, 13, 19 };
      int result = SearchAlgorithms.BinarySearchDisplay(sortedArray, 11);
      Assert.AreEqual(result, 3);
    }
  }
}