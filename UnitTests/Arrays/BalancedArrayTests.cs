﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestHarness.Arrays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHarness.Arrays.Tests
{
  [TestClass()]
  public class BalancedArrayTests
  {
    [TestMethod()]
    public void BalanceArrayTest_BalanceByPrepend()
    {
      int[] a = { 1, 2, 1, 2, 1, 3, };
      int[] result = BalancedArray.BalanceArray(a);
      Assert.AreEqual(result[0], 2);
    }

    [TestMethod()]
    public void BalanceArrayTest_BalanceByAppend()
    {
      int[] a = { 2, 1, 3, 1, 2, 1 };
      int[] result = BalancedArray.BalanceArray(a);
      Assert.AreEqual(result[result.Length - 1], 2);
    }

    [TestMethod()]
    public void BalanceArrayTest_Balanced()
    {
      int[] a = { 2, 1, 3, 3, 1, 2 };
      int[] result = BalancedArray.BalanceArray(a);
      bool equals = a.OrderBy(item => item).SequenceEqual(result.OrderBy(item => item));
      Assert.IsTrue(equals);
    }

  }
}